<?php
include __DIR__ . "/core/System.php";

['file' => $file, 'module_file' => $module_file, 'theme' => $theme] = System::init_web(['WEBDIR' => __DIR__]);

define('BASEADDR', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['SCRIPT_NAME']) . '/');

System::formatDocument($file, $module_file, $theme);

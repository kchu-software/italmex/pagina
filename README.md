# Italmex
### Despues de haber clonado, correr los siguientes comandos para descargar los submodulos:
```
	git submodule update --init --recursive
	git config --global submodule.recurse true
```

### Instalar Docker Desktop:
[Descargar](https://docs.docker.com/desktop/install/windows-install)

### Entrar a la carpeta Core y copiar los archivos "Dockerfile" y "docker-compose.yml" y pegarlos en la carpeta raiz afuera de la carpeta del proyecto.
O usar los siguientes comandos en la terminal:

```
	cd core
	cp ../../Dockerfile
	cp ../../docker-compose.yml
```

### Correr el siguiente comando en la terminal dentro de la carpeta donde esta el archivo docker-compose.yml para levantar la imagen del docker:
```
	docker compose up -d
```

### Instalar el programa NodeJS:
[Descargar](https://nodejs.org/es/download)

### Instalar Yarn con el siguiente comando en la terminal:
```
	npm install --global yarn
```

### Entrar a la carpeta de assets/src dentro de la carpeta del proyecto con el siguiente comando en la terminal:
```
	cd {proyecto}/assets/src
```
### Clonar el archivo settings.json de la carpeta del proyecto con el nombre settings.backup.json y reemplazar el contenido con lo siguiente:
```
	{
  		"apiUrl": "http://localhost/api/"
	}
```
### Instalar los paquetes de NodeJS usando Yarn con el siguiente comando en la terminal:
```
	yarn install
```
### Generar los assets con uno de los siguientes comando en la terminal:
##### Este comando genera assets y espera los siguientes cambios
```
	yarn webpack:watch
```
##### Este comando genera assets para ambiente de desarrollo
```
	yarn webpack:build:dev
```
##### Este comando genera assets para ambiente de produccion
```
	yarn webpack:build:prod
```
<?php

$products = System::curl(["url" => BASEADDR . "ajax/products.json", "full_url" => true]);

?>
<div id='Containerqmxwx' class='Containerqmxwx SPY_vo'>
    <div data-mesh-id='ContainerqmxwxinlineContent' data-testid='inline-content' class=''>
        <div data-mesh-id='ContainerqmxwxinlineContent-gridContainer' data-testid='mesh-container-content'>
            <section id='comp-ldg7el8z' tabindex='-1' class='Oqnisf comp-ldg7el8z wixui-section'>
                <div id='bgLayers_comp-ldg7el8z' data-hook='bgLayers' class='MW5IWV'>
                    <div data-testid='colorUnderlay' class='LWbAav Kv1aVt'></div>
                    <div id='bgMedia_comp-ldg7el8z' class='VgO9Yg'>
                        <wow-image id='img_comp-ldg7el8z' class='HlRz5e Kv1aVt dLPlxY mNGsUM bgImage'
                                   data-image-info='{&quot;containerId&quot;:&quot;comp-ldg7el8z&quot;,&quot;alignType&quot;:&quot;center&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;targetWidth&quot;:980,&quot;targetHeight&quot;:734,&quot;isLQIP&quot;:true,&quot;imageData&quot;:{&quot;width&quot;:5184,&quot;height&quot;:3456,&quot;uri&quot;:&quot;nsplsh_965a9ce3c952437a852b4daa0bb4a762~mv2.jpg&quot;,&quot;name&quot;:&quot;&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}'
                                   data-bg-effect-name='' data-has-ssr-src='true'><img
                                    src='https://static.wixstatic.com/media/nsplsh_965a9ce3c952437a852b4daa0bb4a762~mv2.jpg/v1/fill/w_147,h_98,al_c,q_80,usm_0.66_1.00_0.01,blur_2,enc_auto/nsplsh_965a9ce3c952437a852b4daa0bb4a762~mv2.jpg'
                                    alt='Image by Wouter Supardi Salari'
                                    style='width:100%;height:100%;object-fit:cover;object-position:50% 50%' width='980'
                                    height='734'/></wow-image>
                    </div>
                </div>
                <div data-mesh-id='comp-ldg7el8zinlineContent' data-testid='inline-content' class=''>
                    <div data-mesh-id='comp-ldg7el8zinlineContent-gridContainer' data-testid='mesh-container-content'>
                        <div id='comp-ldg7elcj' class='hFQZVn comp-ldg7elcj'>
                            <div class='nTiihL wixui-box' data-testid='container-bg'></div>
                            <div data-mesh-id='comp-ldg7elcjinlineContent' data-testid='inline-content' class=''>
                                <div data-mesh-id='comp-ldg7elcjinlineContent-gridContainer'
                                     data-testid='mesh-container-content'>
                                    <div id='comp-ldg7elcm1' class='KcpHeO tz5f0K comp-ldg7elcm1 wixui-rich-text'
                                         data-testid='richTextElement'><h1 class='font_0 wixui-rich-text__text'
                                                                           style='font-size:56px; line-height:normal; text-align:center;'>
                                                                            <span style='letter-spacing:normal;' class='wixui-rich-text__text'><span
                                                                                        style='font-size:56px;' class='wixui-rich-text__text'>Aditivos para Alimentos Italmex.</span></span><br
                                                    class='wixui-rich-text__text'>
                                            <span style='font-size:88px;' class='wixui-rich-text__text'>&iexcl;Bienvenido!</span>
                                        </h1></div>
                                    <div id='comp-ldg7elcs' class='KcpHeO tz5f0K comp-ldg7elcs wixui-rich-text'
                                         data-testid='richTextElement'><p class='font_8 wixui-rich-text__text'
                                                                          style='font-size:24px; line-height:1.8em; text-align:center;'>
                                                                            <span style='font-size:24px;' class='wixui-rich-text__text'><span
                                                                                        style='font-weight:bold;' class='wixui-rich-text__text'><span
                                                                                            style='font-family:didot-w01-italic,didot-w05-italic,serif;'
                                                                                            class='wixui-rich-text__text'><span style='letter-spacing:normal;'
                                                                                                                                class='wixui-rich-text__text'>Lo tradicional de nuestro pa&iacute;s, la tortilla de ma&iacute;z.</span></span></span></span>
                                        </p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id='comp-lbciijiy' tabindex='-1' class='Oqnisf comp-lbciijiy wixui-section'>
                <div id='bgLayers_comp-lbciijiy' data-hook='bgLayers' class='MW5IWV'>
                    <div data-testid='colorUnderlay' class='LWbAav Kv1aVt'></div>
                    <div id='bgMedia_comp-lbciijiy' class='VgO9Yg'></div>
                </div>
                <div data-mesh-id='comp-lbciijiyinlineContent' data-testid='inline-content' class=''>
                    <div data-mesh-id='comp-lbciijiyinlineContent-gridContainer' data-testid='mesh-container-content'>
                        <section id='comp-lbciie312' class='comp-lbciie312 CohWsy wixui-column-strip'>
                            <div id='bgLayers_comp-lbciie312' data-hook='bgLayers' class='if7Vw2'>
                                <div data-testid='colorUnderlay' class='tcElKx i1tH8h'></div>
                                <div id='bgMedia_comp-lbciie312' class='wG8dni'></div>
                            </div>
                            <div data-testid='columns' class='V5AUxf'>
                                <div id='comp-lbciie32' class='comp-lbciie32 YzqVVZ wixui-column-strip__column'>
                                    <div id='bgLayers_comp-lbciie32' data-hook='bgLayers' class='MW5IWV'>
                                        <div data-testid='colorUnderlay' class='LWbAav Kv1aVt'></div>
                                        <div id='bgMedia_comp-lbciie32' class='VgO9Yg'></div>
                                    </div>
                                    <div data-mesh-id='comp-lbciie32inlineContent' data-testid='inline-content' class=''>
                                        <div data-mesh-id='comp-lbciie32inlineContent-gridContainer'
                                             data-testid='mesh-container-content'>
                                            <section id='comp-lbciie321' class='comp-lbciie321 CohWsy wixui-column-strip'>
                                                <div id='bgLayers_comp-lbciie321' data-hook='bgLayers' class='if7Vw2'>
                                                    <div data-testid='colorUnderlay' class='tcElKx i1tH8h'></div>
                                                    <div id='bgMedia_comp-lbciie321' class='wG8dni'></div>
                                                </div>
                                                <div data-testid='columns' class='V5AUxf'>
                                                    <div id='comp-lbciie322'
                                                         class='comp-lbciie322 YzqVVZ wixui-column-strip__column'>
                                                        <div id='bgLayers_comp-lbciie322' data-hook='bgLayers'
                                                             class='MW5IWV'>
                                                            <div id='bgMedia_comp-lbciie322' class='VgO9Yg'>
                                                                <wix-video id='videoContainer_comp-lbciie322'
                                                                           data-video-info='{&quot;fittingType&quot;:&quot;fill&quot;,&quot;alignType&quot;:&quot;center&quot;,&quot;hasBgScrollEffect&quot;:&quot;&quot;,&quot;bgEffectName&quot;:&quot;&quot;,&quot;videoId&quot;:&quot;11062b_696e66ac2dd040b88d58853f7998765a&quot;,&quot;videoWidth&quot;:1920,&quot;videoHeight&quot;:1080,&quot;qualities&quot;:[{&quot;quality&quot;:&quot;480p&quot;,&quot;size&quot;:409920,&quot;url&quot;:&quot;video/11062b_696e66ac2dd040b88d58853f7998765a/480p/mp4/file.mp4&quot;},{&quot;quality&quot;:&quot;720p&quot;,&quot;size&quot;:921600,&quot;url&quot;:&quot;video/11062b_696e66ac2dd040b88d58853f7998765a/720p/mp4/file.mp4&quot;},{&quot;quality&quot;:&quot;1080p&quot;,&quot;size&quot;:2073600,&quot;url&quot;:&quot;video/11062b_696e66ac2dd040b88d58853f7998765a/1080p/mp4/file.mp4&quot;}],&quot;isVideoDataExists&quot;:&quot;1&quot;,&quot;videoFormat&quot;:&quot;mp4&quot;,&quot;playbackRate&quot;:1,&quot;autoPlay&quot;:true,&quot;containerId&quot;:&quot;comp-lbciie322&quot;,&quot;animatePoster&quot;:&quot;none&quot;}'
                                                                           class='bX9O_S bgVideo yK6aSC'>
                                                                    <video id='comp-lbciie322_video' class='K8MSra'
                                                                           role='presentation' crossorigin='anonymous'
                                                                           playsinline='' preload='auto' muted='' loop=''
                                                                           tabindex='-1'></video>
                                                                    <wow-image id='comp-lbciie322_img'
                                                                               class='HlRz5e Z_wCwr Jxk_UL bgVideoposter yK6aSC'
                                                                               data-image-info='{&quot;containerId&quot;:&quot;comp-lbciie322&quot;,&quot;alignType&quot;:&quot;center&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;targetWidth&quot;:980,&quot;targetHeight&quot;:540,&quot;isLQIP&quot;:true,&quot;imageData&quot;:{&quot;width&quot;:1920,&quot;height&quot;:1080,&quot;uri&quot;:&quot;11062b_696e66ac2dd040b88d58853f7998765af000.jpg&quot;,&quot;name&quot;:&quot;&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;quality&quot;:{&quot;unsharpMask&quot;:{&quot;radius&quot;:0.33,&quot;amount&quot;:1,&quot;threshold&quot;:0}},&quot;devicePixelRatio&quot;:1}}'
                                                                               data-bg-effect-name=''
                                                                               data-has-ssr-src='true'><img
                                                                                src='https://static.wixstatic.com/media/11062b_696e66ac2dd040b88d58853f7998765af000.jpg/v1/fill/w_144,h_81,al_c,q_80,usm_0.66_1.00_0.01,blur_2,enc_auto/11062b_696e66ac2dd040b88d58853f7998765af000.jpg'
                                                                                alt=''
                                                                                style='width:100%;height:100%;object-fit:cover;object-position:50% 50%'
                                                                                width='980' height='540'/></wow-image>
                                                                </wix-video>
                                                            </div>
                                                        </div>
                                                        <div data-mesh-id='comp-lbciie322inlineContent'
                                                             data-testid='inline-content' class=''>
                                                            <div data-mesh-id='comp-lbciie322inlineContent-gridContainer'
                                                                 data-testid='mesh-container-content'>
                                                                <div id='comp-lbciie323'
                                                                     class='KcpHeO tz5f0K comp-lbciie323 wixui-rich-text'
                                                                     data-testid='richTextElement'><h2
                                                                            class='font_4 wixui-rich-text__text'
                                                                            style='line-height:1.35em; text-align:center; font-size:40px;'>
                                                                                                        <span style='letter-spacing:normal;'
                                                                                                              class='wixui-rich-text__text'><span
                                                                                                                    class='color_11 wixui-rich-text__text'><span
                                                                                                                        style='background-color:#000000;'
                                                                                                                        class='wixui-rich-text__text'>Sobre Aditivos Para Alimentos Italmex</span></span></span>
                                                                    </h2></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section id='comp-lbciie33' class='comp-lbciie33 CohWsy wixui-column-strip'>
                                                <div id='bgLayers_comp-lbciie33' data-hook='bgLayers' class='if7Vw2'>
                                                    <div data-testid='colorUnderlay' class='tcElKx i1tH8h'></div>
                                                    <div id='bgMedia_comp-lbciie33' class='wG8dni'></div>
                                                </div>
                                                <div data-testid='columns' class='V5AUxf'>
                                                    <div id='comp-lbciie331'
                                                         class='comp-lbciie331 YzqVVZ wixui-column-strip__column'>
                                                        <div id='bgLayers_comp-lbciie331' data-hook='bgLayers'
                                                             class='MW5IWV'>
                                                            <div data-testid='colorUnderlay' class='LWbAav Kv1aVt'></div>
                                                            <div id='bgMedia_comp-lbciie331' class='VgO9Yg'></div>
                                                        </div>
                                                        <div data-mesh-id='comp-lbciie331inlineContent'
                                                             data-testid='inline-content' class=''>
                                                            <div data-mesh-id='comp-lbciie331inlineContent-gridContainer'
                                                                 data-testid='mesh-container-content'>
                                                                <div id='comp-lbciie332'
                                                                     class='KcpHeO tz5f0K comp-lbciie332 wixui-rich-text'
                                                                     data-testid='richTextElement'><p
                                                                            class='font_8 wixui-rich-text__text'
                                                                            style='font-size:28px; line-height:1.75em; text-align:center;'>
                                                                                                        <span style='font-size:28px;'
                                                                                                              class='wixui-rich-text__text'><span
                                                                                                                    style='font-weight:bold;'
                                                                                                                    class='wixui-rich-text__text'><span
                                                                                                                        style='letter-spacing:normal;'
                                                                                                                        class='wixui-rich-text__text'><span
                                                                                                                            class='color_15 wixui-rich-text__text'>&iquest;QUI&Eacute;NES SOMOS?</span></span></span></span>
                                                                    </p></div>
                                                                <div id='comp-lbciie333'
                                                                     class='KcpHeO tz5f0K comp-lbciie333 wixui-rich-text'
                                                                     data-testid='richTextElement'><p
                                                                            class='font_9 wixui-rich-text__text'
                                                                            style='font-size:20px; line-height:1.875em; text-align:center;'>
                                                                                                        <span style='font-size:20px;'
                                                                                                              class='wixui-rich-text__text'><span
                                                                                                                    style='font-family:raleway-semibold,raleway,sans-serif;'
                                                                                                                    class='wixui-rich-text__text'><span
                                                                                                                        style='letter-spacing:normal;'
                                                                                                                        class='wixui-rich-text__text'><span
                                                                                                                            class='color_15 wixui-rich-text__text'>Somos una compa&ntilde;&iacute;a fundada en 1998, 100% mexicana. Somos proveedores de ingredientes para la Industria Alimenticia, desde entonces hemos crecido hasta convertirnos en abastecedores a nivel nacional.&nbsp;</span></span></span></span>
                                                                    </p>

                                                                    <p class='font_9 wixui-rich-text__text'
                                                                       style='font-size:20px; line-height:1.875em; text-align:center;'>
                                                                                                            <span style='font-size:20px;'
                                                                                                                  class='wixui-rich-text__text'><span
                                                                                                                        style='font-family:raleway-semibold,raleway,sans-serif;'
                                                                                                                        class='wixui-rich-text__text'><span
                                                                                                                            style='letter-spacing:normal;'
                                                                                                                            class='wixui-rich-text__text'><span
                                                                                                                                class='wixGuard wixui-rich-text__text'>​</span></span></span></span>
                                                                    </p>

                                                                    <p class='font_9 wixui-rich-text__text'
                                                                       style='font-size:20px; line-height:1.875em; text-align:center;'>
                                                                                                            <span style='font-size:20px;'
                                                                                                                  class='wixui-rich-text__text'><span
                                                                                                                        style='font-family:raleway-semibold,raleway,sans-serif;'
                                                                                                                        class='wixui-rich-text__text'><span
                                                                                                                            style='letter-spacing:normal;'
                                                                                                                            class='wixui-rich-text__text'><span
                                                                                                                                class='color_15 wixui-rich-text__text'>Actualmente nos encontramos en LAGOS DE MORENO, JALISCO; contando con distribuidores en diferentes puntos de la Rep&uacute;blica.</span></span></span></span>
                                                                    </p>

                                                                    <p class='font_9 wixui-rich-text__text'
                                                                       style='font-size:20px; line-height:1.875em; text-align:center;'>
                                                                                                            <span style='font-size:20px;'
                                                                                                                  class='wixui-rich-text__text'><span
                                                                                                                        style='font-family:raleway-semibold,raleway,sans-serif;'
                                                                                                                        class='wixui-rich-text__text'><span
                                                                                                                            style='letter-spacing:normal;'
                                                                                                                            class='wixui-rich-text__text'><span
                                                                                                                                class='wixGuard wixui-rich-text__text'>​</span></span></span></span>
                                                                    </p>

                                                                    <p class='font_9 wixui-rich-text__text'
                                                                       style='font-size:20px; line-height:1.875em; text-align:center;'>
                                                                                                            <span style='font-size:20px;'
                                                                                                                  class='wixui-rich-text__text'><span
                                                                                                                        style='font-family:raleway-semibold,raleway,sans-serif;'
                                                                                                                        class='wixui-rich-text__text'><span
                                                                                                                            style='letter-spacing:normal;'
                                                                                                                            class='wixui-rich-text__text'><span
                                                                                                                                class='color_15 wixui-rich-text__text'>El entusiasmo de nuestros empleados y distribuidores, sus actitudes positivas, el esp&iacute;ritu de equipo y la atenci&oacute;n a clientes son los detalles que han formado a ITALMEX, una empresa exitosa.</span></span></span></span>
                                                                    </p>

                                                                    <p class='font_9 wixui-rich-text__text'
                                                                       style='font-size:20px; line-height:1.875em; text-align:center;'>
                                                                                                            <span style='font-size:20px;'
                                                                                                                  class='wixui-rich-text__text'><span
                                                                                                                        style='font-family:raleway-semibold,raleway,sans-serif;'
                                                                                                                        class='wixui-rich-text__text'><span
                                                                                                                            style='letter-spacing:normal;'
                                                                                                                            class='wixui-rich-text__text'><span
                                                                                                                                class='wixGuard wixui-rich-text__text'>​</span></span></span></span>
                                                                    </p>

                                                                    <p class='font_9 wixui-rich-text__text'
                                                                       style='font-size:20px; line-height:1.875em; text-align:center;'>
                                                                                                            <span style='font-size:20px;'
                                                                                                                  class='wixui-rich-text__text'><span
                                                                                                                        style='font-family:raleway-semibold,raleway,sans-serif;'
                                                                                                                        class='wixui-rich-text__text'><span
                                                                                                                            style='letter-spacing:normal;'
                                                                                                                            class='wixui-rich-text__text'><span
                                                                                                                                class='color_15 wixui-rich-text__text'>En ITALMEX nos esforzamos por satisfacer los intereses de calidad, t&eacute;cnicos y econ&oacute;micos de cada uno de nuestros clientes. Con esta experiencia y calidad les proporcionaremos informaci&oacute;n t&eacute;cnica de cada uno de nuestros productos, con el objetivo de lograr mayor productividad y mejorar la calidad en sus productos.&nbsp;</span></span></span></span>
                                                                    </p>

                                                                    <p class='font_9 wixui-rich-text__text'
                                                                       style='font-size:20px; line-height:1.875em; text-align:center;'>
                                                                                                            <span style='font-size:20px;'
                                                                                                                  class='wixui-rich-text__text'><span
                                                                                                                        style='font-family:raleway-semibold,raleway,sans-serif;'
                                                                                                                        class='wixui-rich-text__text'><span
                                                                                                                            style='letter-spacing:normal;'
                                                                                                                            class='wixui-rich-text__text'><span
                                                                                                                                class='wixGuard wixui-rich-text__text'>​</span></span></span></span>
                                                                    </p>

                                                                    <p class='font_9 wixui-rich-text__text'
                                                                       style='line-height:1.875em; text-align:center; font-size:15px;'>
                                                                                                            <span style='font-weight:bold;'
                                                                                                                  class='wixui-rich-text__text'><span
                                                                                                                        style='letter-spacing:normal;'
                                                                                                                        class='wixui-rich-text__text'><span
                                                                                                                            class='color_15 wixui-rich-text__text'>ESTAMOS PARA SERVIRTE CON EL PRODUCTO QUE T&Uacute; NECESITAS.</span></span></span>
                                                                    </p>

                                                                    <p class='font_9 wixui-rich-text__text'
                                                                       style='line-height:1.875em; text-align:center; font-size:15px;'>
                                                                                                            <span style='letter-spacing:normal;'
                                                                                                                  class='wixui-rich-text__text'><span
                                                                                                                        class='wixGuard wixui-rich-text__text'>​</span></span>
                                                                    </p></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
            <section id='comp-ld4ztcmc' tabindex='-1' class='Oqnisf comp-ld4ztcmc wixui-section'>
                <div id='bgLayers_comp-ld4ztcmc' data-hook='bgLayers' class='MW5IWV'>
                    <div data-testid='colorUnderlay' class='LWbAav Kv1aVt'></div>
                    <div id='bgMedia_comp-ld4ztcmc' class='VgO9Yg'></div>
                </div>
                <div data-mesh-id='comp-ld4ztcmcinlineContent' data-testid='inline-content' class=''>
                    <div data-mesh-id='comp-ld4ztcmcinlineContent-gridContainer' data-testid='mesh-container-content'>
                        <div id='comp-ld4ztcn4' class='KcpHeO tz5f0K comp-ld4ztcn4 wixui-rich-text'
                             data-testid='richTextElement'><h2 class='font_0 wixui-rich-text__text'
                                                               style='font-size:70px; text-align:center;'><span
                                        style='font-size:70px;' class='wixui-rich-text__text'><span style='color:#FFFFFF;'
                                                                                                    class='wixui-rich-text__text'>Nuestros productos.</span></span>
                            </h2></div>
                        <section id='comp-ld4ztcn7' class='comp-ld4ztcn7 CohWsy wixui-column-strip'>
                            <div id='bgLayers_comp-ld4ztcn7' data-hook='bgLayers' class='if7Vw2'>
                                <div data-testid='colorUnderlay' class='tcElKx i1tH8h'></div>
                                <div id='bgMedia_comp-ld4ztcn7' class='wG8dni'></div>
                            </div>
                            <div data-testid='columns' class='V5AUxf'>
                                <div id='comp-ld4ztcna' class='comp-ld4ztcna YzqVVZ wixui-column-strip__column'>
                                    <div id='bgLayers_comp-ld4ztcna' data-hook='bgLayers' class='MW5IWV'>
                                        <div data-testid='colorUnderlay' class='LWbAav Kv1aVt'></div>
                                        <div id='bgMedia_comp-ld4ztcna' class='VgO9Yg'></div>
                                    </div>
                                    <div data-mesh-id='comp-ld4ztcnainlineContent' data-testid='inline-content' class=''>
                                        <div data-mesh-id='comp-ld4ztcnainlineContent-gridContainer'
                                             data-testid='mesh-container-content'>
                                            <div id='comp-ld4ztcnc1' class='TWFxr5 '>
                                                <div class='comp-ld4ztcnc1'>
                                                    <style>
                                                        .heightByImageRatio::before {
                                                            display: block;

                                                            content: '';
                                                        }

                                                        .heightByImageRatio0::before {
                                                            padding-top: calc(100% / (3 / 2));
                                                        }

                                                        .heightByImageRatio1::before {
                                                            padding-top: calc(100% / (4 / 3));
                                                        }

                                                        .heightByImageRatio2::before {
                                                            padding-top: calc(100%);
                                                        }

                                                        .heightByImageRatio3::before {
                                                            padding-top: calc(100% / (3 / 4));
                                                        }

                                                        .heightByImageRatio4::before {
                                                            padding-top: calc(100% / (2 / 3));
                                                        }

                                                        .heightByImageRatio5::before {
                                                            padding-top: calc(100% / (16 / 9));
                                                        }

                                                        .heightByImageRatio6::before {
                                                            padding-top: calc(100% / (9 / 16));
                                                        }
                                                    </style>
                                                    <div data-hook='slider-gallery' data-slider-index='0'
                                                         class='lo0oex qe3j8X'>
                                                        <div data-hook='slides-container' class='s5QqWI FdnK0d'>
                                                            <div class='slick-slider lA18NV bE53qy'
                                                                 dir='ltr'>

                                                                <?php
                                                                $width = round(100 / (count($products) ?: 1), 15);
                                                                foreach ($products as $index => $item):
                                                                    ?>
                                                                    <?php
                                                                    $current = $index != 0 ?: 'slick-current';
                                                                    $active = $index > 5 ?: 'slick-active';
                                                                    ?>
                                                                    <div data-index='<?= $index ?>' class='slick-slide <?= $active ?> <?= $current ?>'
                                                                         tabindex='-1' aria-hidden='false'
                                                                         style='outline:none;width:<?= $width ?>%'>
                                                                        <div>
                                                                            <div class='rXdO4y' tabindex='-1'
                                                                                 style='width:100%;display:inline-block'>
                                                                                <div data-hook='slider-gallery-slide'
                                                                                     class='JkMfqr' style='height:100%'>
                                                                                    <div data-slug='<?= $item['slug'] ?>'
                                                                                         role='group'
                                                                                         aria-label='Galería de <?= $item['name'] ?>. <?= $item['description'] ?>'
                                                                                         style='height:100%'
                                                                                         data-hook='product-item-root'
                                                                                         class='ETPbIy'>
                                                                                        <a
                                                                                                href='product-page/<?= $item['slug'] ?>'
                                                                                                tabindex='-1'
                                                                                                class='oQUvqL x5qIv3'
                                                                                                data-hook='product-item-container'>
                                                                                            <div class='mS0yET heightByImageRatio heightByImageRatio2'
                                                                                                 aria-live='assertive'
                                                                                                 data-hook='ProductMediaDataHook.Images'>
                                                                                                <div class='naMHY_ vALCqq'
                                                                                                     data-hook='ImageUiTpaWrapperDataHook.Wrapper_0'>
                                                                                                    <div data-source-width='1080'
                                                                                                         data-source-height='1080'
                                                                                                         data-resize='cover'
                                                                                                         data-use-lqip='true'
                                                                                                         data-is-seo-bot='false'
                                                                                                         class='s__24dbmZ o__5mv6i4---resize-5-cover o__5mv6i4--fluid o__5mv6i4--stretchImage o__5mv6i4--verticalContainer s__65YAB3 v_lwe5'
                                                                                                         data-hook='ImageUiTpaWrapperDataHook.Media_0'>
                                                                                                        <img src='public/img/<?= $item['img'] ?>'
                                                                                                             alt='<?= $item['slug'] ?>'
                                                                                                             style='width:100%;height:100%;object-fit:cover;object-position:50% 50%'/>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class='KSFKAN'
                                                                                                     style='display:var(--shouldShowRibbonOnImage-display, inherit)'>
                                                                                                    <div class='INg0tB FbHYze'
                                                                                                         data-hook='RibbonDataHook.RibbonOnImage'>
                                                                                                        <?= $item['description'] ?>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </a>
                                                                                        <div data-hook='not-image-container'
                                                                                             class='CZ0KIs'>
                                                                                            <div class='A4k3VP'><a
                                                                                                        href='product-page/<?= $item['slug'] ?>'
                                                                                                        class='JPDEZd'
                                                                                                        data-hook='product-item-product-details-link'>
                                                                                                    <div style='display:var(--showProductDetails-display, inherit)'>
                                                                                                        <div class='t2u_rw'
                                                                                                             data-hook='product-item-product-details'>
                                                                                                            <div style='display:var(--gallery_showProductName-display, inherit)'>
                                                                                                                <p class='ss8gI2T oyuhkAa---typography-11-runningText oyuhkAa---priority-7-primary syHtuvM FzO_a9'
                                                                                                                   aria-hidden='false'
                                                                                                                   data-hook='product-item-name'>
                                                                                                                    <?= $item['name'] ?>
                                                                                                                </p>
                                                                                                            </div>
                                                                                                            <div style='display:var(--gallery_showDiscountName-display, inherit)'></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </a>
                                                                                                <div class='PgHPAM'>
                                                                                                    <div data-hook='slot-placeholder-comp-ld4ztcnc1.product-gallery-details-slot-1'
                                                                                                         id='comp-ld4ztcnc1.product-gallery-details-slot-1'
                                                                                                         class=''></div>
                                                                                                </div>
                                                                                                <div></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php endforeach; ?>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
            <section id='comp-lbciik76' tabindex='-1' class='Oqnisf comp-lbciik76 wixui-section'>
                <div id='bgLayers_comp-lbciik76' data-hook='bgLayers' class='MW5IWV'>
                    <div data-testid='colorUnderlay' class='LWbAav Kv1aVt'></div>
                    <div id='bgMedia_comp-lbciik76' class='VgO9Yg'></div>
                </div>
                <div data-mesh-id='comp-lbciik76inlineContent' data-testid='inline-content' class=''>
                    <div data-mesh-id='comp-lbciik76inlineContent-gridContainer' data-testid='mesh-container-content'>
                        <section id='comp-lbciie3a3' class='comp-lbciie3a3 CohWsy wixui-column-strip'>
                            <div id='bgLayers_comp-lbciie3a3' data-hook='bgLayers' class='if7Vw2'>
                                <div data-testid='colorUnderlay' class='tcElKx i1tH8h'></div>
                                <div id='bgMedia_comp-lbciie3a3' class='wG8dni'></div>
                            </div>
                            <div data-testid='columns' class='V5AUxf'>
                                <div id='comp-lbciie3a4' class='comp-lbciie3a4 YzqVVZ wixui-column-strip__column'>
                                    <div id='bgLayers_comp-lbciie3a4' data-hook='bgLayers' class='MW5IWV'>
                                        <div data-testid='colorUnderlay' class='LWbAav Kv1aVt'></div>
                                        <div id='bgMedia_comp-lbciie3a4' class='VgO9Yg'></div>
                                    </div>
                                    <div data-mesh-id='comp-lbciie3a4inlineContent' data-testid='inline-content' class=''>
                                        <div data-mesh-id='comp-lbciie3a4inlineContent-gridContainer'
                                             data-testid='mesh-container-content'>
                                            <section id='comp-lbciie3b' class='comp-lbciie3b CohWsy wixui-column-strip'>
                                                <div id='bgLayers_comp-lbciie3b' data-hook='bgLayers' class='if7Vw2'>
                                                    <div data-testid='colorUnderlay' class='tcElKx i1tH8h'></div>
                                                    <div id='bgMedia_comp-lbciie3b' class='wG8dni'></div>
                                                </div>
                                                <div data-testid='columns' class='V5AUxf'>
                                                    <div id='comp-lbciie3b1'
                                                         class='comp-lbciie3b1 YzqVVZ wixui-column-strip__column'>
                                                        <div id='bgLayers_comp-lbciie3b1' data-hook='bgLayers'
                                                             class='MW5IWV'>
                                                            <div data-testid='colorUnderlay' class='LWbAav Kv1aVt'></div>
                                                            <div id='bgMedia_comp-lbciie3b1' class='VgO9Yg'></div>
                                                        </div>
                                                        <div data-mesh-id='comp-lbciie3b1inlineContent'
                                                             data-testid='inline-content' class=''>
                                                            <div data-mesh-id='comp-lbciie3b1inlineContent-gridContainer'
                                                                 data-testid='mesh-container-content'>
                                                                <div id='comp-lbciie3b2'
                                                                     class='BaOVQ8 tz5f0K comp-lbciie3b2 wixui-rich-text'
                                                                     data-testid='richTextElement'><h2
                                                                            class='font_4 wixui-rich-text__text'
                                                                            style='text-align:center; line-height:1.35em; font-size:40px;'>
                                                                        <span class='color_11 wixui-rich-text__text'>Contáctanos</span>
                                                                    </h2></div>
                                                                <div id="comp-lbciie3b3" class="KcpHeO tz5f0K comp-lbciie3b3 wixui-rich-text" data-testid="richTextElement">
                                                                    <p class="font_9 wixui-rich-text__text" style="line-height:1.875em; text-align:center; font-size:17px;">
                                                                        <span style="font-size:17px;" class="wixui-rich-text__text">
                                                                            <span style="letter-spacing:normal;" class="wixui-rich-text__text">
                                                                                <span class="color_11 wixui-rich-text__text">&Eacute;bano 161-A, Lomas del Valle, 47460 Lagos de Moreno, Jal., M&eacute;xico</span>
                                                                            </span>
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <div id="comp-lbciie3c"
                                                                     class="KcpHeO tz5f0K comp-lbciie3c wixui-rich-text"
                                                                     data-testid="richTextElement"><p
                                                                            class="font_9 wixui-rich-text__text"
                                                                            style="font-size:16px; line-height:1.875em; text-align:center;">
                                                                        <a href="mailto:italmex070@gmail.com" target="_self"
                                                                           class="wixui-rich-text__text"><span
                                                                                    style="font-size:16px;"
                                                                                    class="wixui-rich-text__text"><span
                                                                                        style="letter-spacing:normal;"
                                                                                        class="wixui-rich-text__text"><span
                                                                                            class="color_11 wixui-rich-text__text"><span
                                                                                                style="text-decoration:underline;"
                                                                                                class="wixui-rich-text__text">italmex70@hotmail.com</span></span></span></span></a>
                                                                    </p></div>
                                                                <div id="comp-ldg79epu" class="comp-ldg79epu"
                                                                     data-semantic-classname="button">
                                                                    <button type="button"
                                                                            class="StylableButton2545352419__root style-ldg79esc__root wixui-button"
                                                                            data-testid="buttonContent" aria-label="Mail">
                                                                        <div class="StylableButton2545352419__container">
                                                                                                                <span class="StylableButton2545352419__label wixui-button__label"
                                                                                                                      data-testid="stylablebutton-label">Mail</span><span
                                                                                    class="StylableButton2545352419__icon wixui-button__icon"
                                                                                    aria-hidden="true"
                                                                                    data-testid="stylablebutton-icon"><div>
                                                                                                                <svg data-bbox="20 35 159.999 130"
                                                                                                                     viewBox="0 0 200 200" height="200"
                                                                                                                     width="200"
                                                                                                                     xmlns="http://www.w3.org/2000/svg"
                                                                                                                     data-type="shape">
    <g>
        <path d="M160.062 165H39.936C28.942 165 20 156.055 20 145.058V54.943C20.001 43.946 28.943 35 39.936 35h120.125c10.994 0 19.938 8.946 19.938 19.943v90.116c0 10.996-8.944 19.941-19.937 19.941zM39.936 44.845c-5.565 0-10.093 4.53-10.093 10.098v90.116c0 5.568 4.528 10.097 10.093 10.097h120.125c5.567 0 10.095-4.529 10.095-10.097V54.943c0-5.568-4.528-10.098-10.095-10.098H39.936z"/>
        <path d="M100 112.433a4.914 4.914 0 0 1-2.822-.89L22.1 58.975a4.922 4.922 0 0 1 5.643-8.065L100 101.502l72.257-50.591a4.922 4.922 0 0 1 5.643 8.065l-75.078 52.567a4.914 4.914 0 0 1-2.822.89z"/>
    </g>
</svg>
</div></span></div>
                                                                    </button>
                                                                </div>
                                                                <div id="comp-lbciie3c1"
                                                                     class="KcpHeO tz5f0K comp-lbciie3c1 wixui-rich-text"
                                                                     data-testid="richTextElement"><p
                                                                            class="font_9 wixui-rich-text__text"
                                                                            style="font-size:16px; line-height:1.875em; text-align:center;">
                                                                        <a href="tel:4747469438"
                                                                           class="wixui-rich-text__text"><span
                                                                                    style="font-weight:bold;"
                                                                                    class="wixui-rich-text__text"><span
                                                                                        style="font-size:16px;"
                                                                                        class="wixui-rich-text__text"><span
                                                                                            style="letter-spacing:normal;"
                                                                                            class="wixui-rich-text__text"><span
                                                                                                class="color_11 wixui-rich-text__text"><span
                                                                                                    style="text-decoration:underline;"
                                                                                                    class="wixui-rich-text__text">474 746 9438</span></span></span></span></span></a>
                                                                    </p>

                                                                    <p class="font_9 wixui-rich-text__text" style="font-size:16px; line-height:1.875em; text-align:center;">
                                                                        <span style="font-weight:bold;" class="wixui-rich-text__text">
                                                                            <span style="font-size:16px;" class="wixui-rich-text__text">
                                                                                <span style="letter-spacing:normal;" class="wixui-rich-text__text">
                                                                                    <span class="color_11 wixui-rich-text__text">
                                                                                        <span style="text-decoration:underline;" class="wixui-rich-text__text">474 403 2909</span>
                                                                                    </span>
                                                                                </span>
                                                                            </span>
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <div id="comp-ldg6zhdk" class="comp-ldg6zhdk"
                                                                     data-semantic-classname="button"><a
                                                                            data-testid="linkElement" href="tel:4747469438"
                                                                            class="StylableButton2545352419__root style-ldg6zhh1__root wixui-button StylableButton2545352419__link"
                                                                            aria-label="Phone">
                                                                        <div class="StylableButton2545352419__container"><span
                                                                                    class="StylableButton2545352419__label wixui-button__label"
                                                                                    data-testid="stylablebutton-label">Phone</span><span
                                                                                    class="StylableButton2545352419__icon wixui-button__icon"
                                                                                    aria-hidden="true"
                                                                                    data-testid="stylablebutton-icon"><div>
                                                                                                            <svg data-bbox="20.001 20 159.998 160.001"
                                                                                                                 viewBox="0 0 200 200" height="200" width="200"
                                                                                                                 xmlns="http://www.w3.org/2000/svg"
                                                                                                                 data-type="shape">
    <g>
        <path d="M159.449 180c-.634 0-1.26-.028-1.88-.084-23.876-2.589-47.047-10.815-67.108-23.8-18.6-11.798-34.717-27.886-46.564-46.495-13.03-20.074-21.274-43.307-23.811-67.147-1.027-11.366 7.334-21.371 18.631-22.39a20.688 20.688 0 0 1 1.839-.083h22.441l.2-.001c10.15 0 18.89 7.56 20.321 17.663a90.246 90.246 0 0 0 4.936 19.805c2.813 7.467.996 15.971-4.631 21.655l-6.441 6.426a114.027 114.027 0 0 0 36.943 36.871l6.414-6.404c5.713-5.642 14.238-7.457 21.73-4.642a90.695 90.695 0 0 0 19.778 4.916c10.37 1.462 17.968 10.383 17.747 20.778v22.335c.046 11.301-9.146 20.552-20.491 20.598l-.054-.001zM63.145 31.209l-.094.001H40.555c-.278 0-.555.013-.833.038-5.125.462-8.923 5.007-8.46 10.131 2.341 22.008 9.982 43.543 22.085 62.188 10.995 17.272 25.917 32.166 43.182 43.116 18.638 12.063 40.114 19.689 62.151 22.079.192.018.593.049.785.029 5.144-.022 9.321-4.223 9.3-9.366v-22.421a9.339 9.339 0 0 0-8.038-9.609 101.852 101.852 0 0 1-22.198-5.524 9.393 9.393 0 0 0-9.871 2.094l-9.477 9.46a5.614 5.614 0 0 1-6.746.909 125.228 125.228 0 0 1-46.989-46.9 5.592 5.592 0 0 1 .91-6.733l9.501-9.483c2.535-2.559 3.358-6.422 2.079-9.819a101.42 101.42 0 0 1-5.545-22.22c-.643-4.537-4.613-7.97-9.246-7.97z"/>
    </g>
</svg>
</div></span></div>
                                                                    </a></div>
                                                                <div id="comp-ldg73wog"
                                                                     class="MazNVa comp-ldg73wog wixui-image">
                                                                    <div data-testid="linkElement" class="j7pOnl">
                                                                        <wow-image id="img_comp-ldg73wog"
                                                                                   class="HlRz5e BI8PVQ"
                                                                                   data-image-info="{&quot;containerId&quot;:&quot;comp-ldg73wog&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;targetWidth&quot;:40,&quot;targetHeight&quot;:40,&quot;isLQIP&quot;:true,&quot;imageData&quot;:{&quot;width&quot;:1000,&quot;height&quot;:1000,&quot;uri&quot;:&quot;9d7583_8b7facfcad8e4e2cbe514ae0de21bd50~mv2.png&quot;,&quot;name&quot;:&quot;580b57fcd9996e24bc43c543.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}"
                                                                                   data-bg-effect-name=""
                                                                                   data-has-ssr-src="true"><img
                                                                                    src="https://static.wixstatic.com/media/9d7583_8b7facfcad8e4e2cbe514ae0de21bd50~mv2.png/v1/fill/w_40,h_40,al_c,q_85,usm_0.66_1.00_0.01,blur_3,enc_auto/580b57fcd9996e24bc43c543.png"
                                                                                    alt="580b57fcd9996e24bc43c543.png"
                                                                                    style="width:100%;height:100%;object-fit:cover;object-position:50% 50%"
                                                                                    width="40" height="40"/></wow-image>
                                                                    </div>
                                                                </div>
                                                                <div id="comp-ldg76p03"
                                                                     class="KcpHeO tz5f0K comp-ldg76p03 wixui-rich-text"
                                                                     data-testid="richTextElement"><p
                                                                            class="font_9 wixui-rich-text__text"
                                                                            style="font-size:16px; line-height:1.875em; text-align:center;">
                                                                        <a href="tel:4772645238"
                                                                           class="wixui-rich-text__text"><span
                                                                                    style="font-weight:bold;"
                                                                                    class="wixui-rich-text__text"><span
                                                                                        style="font-size:16px;"
                                                                                        class="wixui-rich-text__text"><span
                                                                                            style="letter-spacing:normal;"
                                                                                            class="wixui-rich-text__text"><span
                                                                                                class="color_11 wixui-rich-text__text"><span
                                                                                                    style="text-decoration:underline;"
                                                                                                    class="wixui-rich-text__text">477 264 5238</span></span></span></span></span></a>
                                                                    </p></div>
                                                                <div id="comp-lddkhpym" class="comp-lddkhpym WzbAF8">
                                                                    <ul class="mpGTIt" aria-label="Social Bar">
                                                                        <li id="dataItem-lddkhq6w-comp-lddkhpym"
                                                                            class="O6KwRn"><a data-testid="linkElement"
                                                                                              href="https://www.facebook.com/profile.php?id=100066822833154"
                                                                                              target="_blank"
                                                                                              rel="noreferrer noopener"
                                                                                              class="oRtuWN">
                                                                                <wow-image id="img_0_comp-lddkhpym"
                                                                                           class="HlRz5e YaS0jR"
                                                                                           data-image-info="{&quot;containerId&quot;:&quot;dataItem-lddkhq6w-comp-lddkhpym&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:200,&quot;height&quot;:200,&quot;uri&quot;:&quot;e316f544f9094143b9eac01f1f19e697.png&quot;,&quot;name&quot;:&quot;&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}"
                                                                                           data-bg-effect-name=""
                                                                                           data-has-ssr-src=""
                                                                                           style="--wix-img-max-width:max(200px, 100%)">
                                                                                    <img alt="Facebook"/></wow-image>
                                                                            </a></li>
                                                                    </ul>
                                                                </div>
                                                                <div id="comp-ldg6p2ap"
                                                                     class="KcpHeO tz5f0K comp-ldg6p2ap wixui-rich-text"
                                                                     data-testid="richTextElement"><p
                                                                            class="font_9 wixui-rich-text__text"
                                                                            style="line-height:1.875em; text-align:center; font-size:12px;">
                                                                                                        <span style="font-size:12px;"
                                                                                                              class="wixui-rich-text__text"><span
                                                                                                                    style="font-family:lulo-clean-w01-one-bold,lulo-clean-w05-one-bold,sans-serif;"
                                                                                                                    class="wixui-rich-text__text"><span
                                                                                                                        style="letter-spacing:normal;"
                                                                                                                        class="wixui-rich-text__text"><span
                                                                                                                            class="color_11 wixui-rich-text__text">&iexcl;B&uacute;scanos!</span></span></span></span>
                                                                    </p>

                                                                    <p class="font_9 wixui-rich-text__text"
                                                                       style="line-height:1.875em; text-align:center; font-size:12px;">
                                                                                                            <span style="font-size:12px;"
                                                                                                                  class="wixui-rich-text__text"><span
                                                                                                                        style="font-family:lulo-clean-w01-one-bold,lulo-clean-w05-one-bold,sans-serif;"
                                                                                                                        class="wixui-rich-text__text"><span
                                                                                                                            style="letter-spacing:normal;"
                                                                                                                            class="wixui-rich-text__text"><span
                                                                                                                                class="color_11 wixui-rich-text__text">Aditivos Para Alimentos Italmex, S.A de C.V.</span></span></span></span>
                                                                    </p></div>
                                                                <div id="comp-ldg6rspi"
                                                                     class="KcpHeO tz5f0K comp-ldg6rspi wixui-rich-text"
                                                                     data-testid="richTextElement"><p
                                                                            class="font_9 wixui-rich-text__text"
                                                                            style="font-size:12px; line-height:1.875em; text-align:center;">
                                                                                                        <span style="font-weight:bold;"
                                                                                                              class="wixui-rich-text__text"><span
                                                                                                                    style="font-size:12px;"
                                                                                                                    class="wixui-rich-text__text"><span
                                                                                                                        style="letter-spacing:normal;"
                                                                                                                        class="wixui-rich-text__text"><span
                                                                                                                            class="color_11 wixui-rich-text__text">O haz click en el &iacute;cono y te mandar&aacute; directo a nuestra p&aacute;gina.</span></span></span></span>
                                                                    </p></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="comp-lbciie3c2"
                                                         class="comp-lbciie3c2 YzqVVZ wixui-column-strip__column">
                                                        <div id="bgLayers_comp-lbciie3c2" data-hook="bgLayers"
                                                             class="MW5IWV">
                                                            <div data-testid="colorUnderlay" class="LWbAav Kv1aVt"></div>
                                                            <div id="bgMedia_comp-lbciie3c2" class="VgO9Yg"></div>
                                                        </div>
                                                        <div data-mesh-id="comp-lbciie3c2inlineContent"
                                                             data-testid="inline-content" class="">
                                                            <div data-mesh-id="comp-lbciie3c2inlineContent-gridContainer"
                                                                 data-testid="mesh-container-content">
                                                                <div id="comp-lbciihap" class="bkIuWA comp-lbciihap">
                                                                    <div data-mesh-id="comp-lbciihapinlineContent"
                                                                         data-testid="inline-content" class="">
                                                                        <div data-mesh-id="comp-lbciihapinlineContent-gridContainer"
                                                                             data-testid="mesh-container-content">
                                                                            <form id="comp-lbciihbg"
                                                                                  class="JVi7i2 comp-lbciihbg wixui-form">
                                                                                <div data-mesh-id="comp-lbciihbginlineContent"
                                                                                     data-testid="inline-content" class="">
                                                                                    <div data-mesh-id="comp-lbciihbginlineContent-gridContainer"
                                                                                         data-testid="mesh-container-content">
                                                                                        <div id="comp-lbciihbt"
                                                                                             class="MpKiNN comp-lbciihbt wixui-text-input qzvPmW lPl_oN">
                                                                                            <label for="input_comp-lbciihbt"
                                                                                                   class="wPeA6j wixui-text-input__label">Nombre</label>
                                                                                            <div class="pUnTVX"><input
                                                                                                        name="nombre"
                                                                                                        id="input_comp-lbciihbt"
                                                                                                        class="KvoMHf has-custom-focus wixui-text-input__input"
                                                                                                        type="text"
                                                                                                        placeholder="Ingresa tu nombre"
                                                                                                        required=""
                                                                                                        aria-required="true"
                                                                                                        maxLength="100"
                                                                                                        value=""/></div>
                                                                                        </div>
                                                                                        <div id="comp-lbciihcc1"
                                                                                             class="MpKiNN comp-lbciihcc1 wixui-text-input qzvPmW">
                                                                                            <label for="input_comp-lbciihcc1"
                                                                                                   class="wPeA6j wixui-text-input__label">Dirección</label>
                                                                                            <div class="pUnTVX"><input
                                                                                                        name="dirección"
                                                                                                        id="input_comp-lbciihcc1"
                                                                                                        class="KvoMHf has-custom-focus wixui-text-input__input"
                                                                                                        type="text"
                                                                                                        placeholder="Ingresa tu dirección"
                                                                                                        aria-required="false"
                                                                                                        maxLength="250"
                                                                                                        value=""/></div>
                                                                                        </div>
                                                                                        <div id="comp-lbciihci"
                                                                                             class="MpKiNN comp-lbciihci wixui-text-input qzvPmW lPl_oN">
                                                                                            <label for="input_comp-lbciihci"
                                                                                                   class="wPeA6j wixui-text-input__label">Email</label>
                                                                                            <div class="pUnTVX"><input
                                                                                                        name="email"
                                                                                                        id="input_comp-lbciihci"
                                                                                                        class="KvoMHf has-custom-focus wixui-text-input__input"
                                                                                                        type="email"
                                                                                                        placeholder="Ingresa tu email"
                                                                                                        required=""
                                                                                                        aria-required="true"
                                                                                                        pattern="^.+@.+\.[a-zA-Z]{2,63}$"
                                                                                                        maxLength="250"
                                                                                                        value=""/></div>
                                                                                        </div>
                                                                                        <div id="comp-lbciihcn"
                                                                                             class="MpKiNN comp-lbciihcn wixui-text-input qzvPmW">
                                                                                            <label for="input_comp-lbciihcn"
                                                                                                   class="wPeA6j wixui-text-input__label">Teléfono</label>
                                                                                            <div class="pUnTVX"><input
                                                                                                        name="phone"
                                                                                                        id="input_comp-lbciihcn"
                                                                                                        class="KvoMHf has-custom-focus wixui-text-input__input"
                                                                                                        type="tel"
                                                                                                        placeholder="Introduce tu número de teléfono"
                                                                                                        aria-required="false"
                                                                                                        maxLength="50" value=""/>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="comp-lbciihcu"
                                                                                             class="MpKiNN comp-lbciihcu wixui-text-input qzvPmW">
                                                                                            <label for="input_comp-lbciihcu"
                                                                                                   class="wPeA6j wixui-text-input__label">Asunto</label>
                                                                                            <div class="pUnTVX"><input
                                                                                                        name="asunto"
                                                                                                        id="input_comp-lbciihcu"
                                                                                                        class="KvoMHf has-custom-focus wixui-text-input__input"
                                                                                                        type="text"
                                                                                                        placeholder="Escribe el asunto"
                                                                                                        aria-required="false"
                                                                                                        value=""/></div>
                                                                                        </div>
                                                                                        <div id="comp-lbciihd0"
                                                                                             class="snt4Te comp-lbciihd0 wixui-text-box oKe0Po">
                                                                                            <label for="textarea_comp-lbciihd0"
                                                                                                   class="PSkPrR wixui-text-box__label">Mensaje</label><textarea
                                                                                                    id="textarea_comp-lbciihd0"
                                                                                                    class="rEindN has-custom-focus wixui-text-box__input"
                                                                                                    placeholder="Escribe tu mensaje aquí..."
                                                                                                    aria-required="false"></textarea>
                                                                                        </div>
                                                                                        <div id="comp-lbciihdg"
                                                                                             class="BaOVQ8 tz5f0K comp-lbciihdg wixui-rich-text"
                                                                                             data-testid="richTextElement"><p
                                                                                                    class="font_8 wixui-rich-text__text"
                                                                                                    style="text-align:center; font-size:18px;">
                                                                                                <span class="color_11 wixui-rich-text__text">¡Gracias por tu mensaje!</span>
                                                                                            </p></div>
                                                                                        <div class="comp-lbciihdk R6ex7N"
                                                                                             id="comp-lbciihdk"
                                                                                             aria-disabled="false">
                                                                                            <button aria-disabled="false"
                                                                                                    data-testid="buttonElement"
                                                                                                    class="kuTaGy wixui-button zKbzSQ">
                                                                                                <span class="M3I7Z2 wixui-button__label">Enviar</span>
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
            <section id="comp-lbciijzb" tabindex="-1" class="Oqnisf comp-lbciijzb wixui-section">
                <div id="bgLayers_comp-lbciijzb" data-hook="bgLayers" class="MW5IWV">
                    <div data-testid="colorUnderlay" class="LWbAav Kv1aVt"></div>
                    <div id="bgMedia_comp-lbciijzb" class="VgO9Yg"></div>
                </div>
                <div data-mesh-id="comp-lbciijzbinlineContent" data-testid="inline-content" class="">
                    <div data-mesh-id="comp-lbciijzbinlineContent-gridContainer" data-testid="mesh-container-content">
                        <section id="comp-lbciie39" class="comp-lbciie39 CohWsy wixui-column-strip">
                            <div id="bgLayers_comp-lbciie39" data-hook="bgLayers" class="if7Vw2">
                                <div data-testid="colorUnderlay" class="tcElKx i1tH8h"></div>
                                <div id="bgMedia_comp-lbciie39" class="wG8dni"></div>
                            </div>
                            <div data-testid="columns" class="V5AUxf">
                                <div id="comp-lbciie391" class="comp-lbciie391 YzqVVZ wixui-column-strip__column">
                                    <div id="bgLayers_comp-lbciie391" data-hook="bgLayers" class="MW5IWV">
                                        <div data-testid="colorUnderlay" class="LWbAav Kv1aVt"></div>
                                        <div id="bgMedia_comp-lbciie391" class="VgO9Yg"></div>
                                    </div>
                                    <div data-mesh-id="comp-lbciie391inlineContent" data-testid="inline-content" class="">
                                        <div data-mesh-id="comp-lbciie391inlineContent-gridContainer"
                                             data-testid="mesh-container-content">
                                            <section id="comp-lbciie392" class="comp-lbciie392 CohWsy wixui-column-strip">
                                                <div id="bgLayers_comp-lbciie392" data-hook="bgLayers" class="if7Vw2">
                                                    <div data-testid="colorUnderlay" class="tcElKx i1tH8h"></div>
                                                    <div id="bgMedia_comp-lbciie392" class="wG8dni"></div>
                                                </div>
                                                <div data-testid="columns" class="V5AUxf">
                                                    <div id="comp-lbciie393"
                                                         class="comp-lbciie393 YzqVVZ wixui-column-strip__column">
                                                        <div id="bgLayers_comp-lbciie393" data-hook="bgLayers"
                                                             class="MW5IWV">
                                                            <div data-testid="colorUnderlay" class="LWbAav Kv1aVt"></div>
                                                            <div id="bgMedia_comp-lbciie393" class="VgO9Yg"></div>
                                                        </div>
                                                        <div data-mesh-id="comp-lbciie393inlineContent"
                                                             data-testid="inline-content" class="">
                                                            <div data-mesh-id="comp-lbciie393inlineContent-gridContainer"
                                                                 data-testid="mesh-container-content">
                                                                <div id="comp-lbciie3a"
                                                                     class="KcpHeO tz5f0K comp-lbciie3a wixui-rich-text"
                                                                     data-testid="richTextElement"><h2
                                                                            class="font_6 wixui-rich-text__text"
                                                                            style="font-size:28px; line-height:1.41em; text-align:center;">
                                                                                                        <span style="font-size:28px;"
                                                                                                              class="wixui-rich-text__text"><span
                                                                                                                    style="font-weight:bold;"
                                                                                                                    class="wixui-rich-text__text"><span
                                                                                                                        style="letter-spacing:normal;"
                                                                                                                        class="wixui-rich-text__text"><span
                                                                                                                            class="color_18 wixui-rich-text__text">Horario de atenci&oacute;n:</span></span></span></span>
                                                                    </h2></div>
                                                                <div id="comp-lbciie3a1"
                                                                     class="KcpHeO tz5f0K comp-lbciie3a1 wixui-rich-text"
                                                                     data-testid="richTextElement"><p
                                                                            class="font_8 wixui-rich-text__text"
                                                                            style="line-height:1.75em; text-align:center; font-size:20px;">
                                                                                                        <span style="font-size:20px;"
                                                                                                              class="wixui-rich-text__text"><span
                                                                                                                    style="font-style:italic;"
                                                                                                                    class="wixui-rich-text__text"><span
                                                                                                                        style="letter-spacing:normal;"
                                                                                                                        class="wixui-rich-text__text"><span
                                                                                                                            class="color_15 wixui-rich-text__text">Vis&iacute;tanos</span></span></span></span>
                                                                    </p></div>
                                                                <div id="comp-lbciie3a2"
                                                                     class="KcpHeO tz5f0K comp-lbciie3a2 wixui-rich-text"
                                                                     data-testid="richTextElement"><p
                                                                            class="font_9 wixui-rich-text__text"
                                                                            style="font-size:22px; line-height:1.875em; text-align:center;">
                                                                                                        <span style="font-size:22px;"
                                                                                                              class="wixui-rich-text__text"><span
                                                                                                                    style="letter-spacing:normal;"
                                                                                                                    class="wixui-rich-text__text"><span
                                                                                                                        class="color_15 wixui-rich-text__text">Lunes - Viernes: 9:00 am. - 5:30 p.m.</span></span></span>
                                                                    </p>

                                                                    <p class="font_9 wixui-rich-text__text"
                                                                       style="font-size:22px; line-height:1.875em; text-align:center;">
                                                                        &nbsp;</p>

                                                                    <p class="font_9 wixui-rich-text__text"
                                                                       style="font-size:22px; line-height:1.875em; text-align:center;">
                                                                        <br class="wixui-rich-text__text">
                                                                        <span style="font-size:22px;"
                                                                              class="wixui-rich-text__text"><span
                                                                                    style="letter-spacing:normal;"
                                                                                    class="wixui-rich-text__text"><span
                                                                                        class="color_15 wixui-rich-text__text">S&aacute;bado: 9:00 am. - 2:00 p.m.</span></span></span>
                                                                    </p>

                                                                    <p class="font_9 wixui-rich-text__text"
                                                                       style="font-size:22px; line-height:1.875em; text-align:center;">
                                                                        <br class="wixui-rich-text__text">
                                                                        <span style="font-size:22px;"
                                                                              class="wixui-rich-text__text"><span
                                                                                    style="letter-spacing:normal;"
                                                                                    class="wixui-rich-text__text"><span
                                                                                        class="wixGuard wixui-rich-text__text">​</span></span></span>
                                                                    </p></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<style>
    .slick-arrow {
        padding: 0;
        overflow: hidden;
        cursor: pointer;
        background-color: rgba(0, 0, 0, 0);
        background-repeat: no-repeat;
        border: none;
        outline: none;
    }
    .slick-arrow:before{
        color:black;
    }
</style>
<script>
    $(() => {
        $('.slick-slider').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1
        });
    });
</script>
